
# constants
# ---------------------------------------------------------------------------
JSON_SPLIT_TOOL="${SCRIPT_DIR}/json-split.awk"
STREAM_PRINTF_TOOL="${SCRIPT_DIR}/stream-printf.awk"

declare -A OBJECT_FILE_FMT
OBJECT_FILE_FMT[domain]="domains/%s-domain.json"
OBJECT_FILE_FMT[user]="domains/%s/users/%s-user.json"
OBJECT_FILE_FMT[project]="domains/%s/projects/%s-project.json"
OBJECT_FILE_FMT[quota]="domains/%s/quotas/%s-quota.json"
OBJECT_FILE_FMT[server]="domains/%s/projects/%s/servers/%s-server.json"
OBJECT_FILE_FMT[flavor]="flavors/%s-flavor.json"
OBJECT_FILE_FMT[image]="images/%s-image.json"
OBJECT_FILE_FMT[mapping]="mappings/%s-mapping.json"
OBJECT_FILE_FMT[volume]="volumes/%s-volume.json"
OBJECT_FILE_FMT[service]="services/%s-service.json"
OBJECT_FILE_FMT[region]="regions/%s-region.json"
OBJECT_FILE_FMT[hypervisor]="hypervisors/%s-hypervisor.json"
OBJECT_FILE_FMT[loadbalancer]="domains/%s/projects/%s/loadbalancers/%s-loadbalancer.json"
OBJECT_FILE_FMT[network]="domains/%s/projects/%s/networks/%s-network.json"
OBJECT_FILE_FMT[subnet]="domains/%s/projects/%s/subnets/%s-subnet.json"
OBJECT_FILE_FMT[floating-ip]="domains/%s/projects/%s/floating-ips/%s-floating-ip.json"
OBJECT_FILE_FMT[router]="domains/%s/projects/%s/routers/%s-router.json"
OBJECT_FILE_FMT[volume-type]="volume-types/%s-volume-type.json"
OBJECT_FILE_FMT[ip-availability]="ip-availabilities/%s-ip-availability.json"
OBJECT_FILE_FMT[role]="roles/%s-role.json"
OBJECT_FILE_FMT[endpoint]="endpoints/%s-endpoint.json"
OBJECT_FILE_FMT[aggregate]="aggregates/%s-aggregate.json"
OBJECT_FILE_FMT[catalog]="catalogs/%s-catalog.json"
OBJECT_FILE_FMT[host]="hosts/%s-host.json"
OBJECT_FILE_FMT[identity-provider]="identity-providers/%s-identity-provider.json"
OBJECT_FILE_FMT[usage-last-1mo]="usages/usage-last-1mo.json"
OBJECT_FILE_FMT[usage-last-3mo]="usages/usage-last-3mo.json"
OBJECT_FILE_FMT[usage-last-6mo]="usages/usage-last-6mo.json"
OBJECT_FILE_FMT[usage-last-12mo]="usages/usage-last-12mo.json"
OBJECT_FILE_FMT[usage-ytd]="usages/usage-ytd.json"
OBJECT_FILE_FMT[availability-zone]="availability-zones/%s-availability-zone.json"
OBJECT_FILE_FMT[network-agent]="network-agents/%s-network-agent.json"
OBJECT_FILE_FMT[port]="ports/%s-port.json"

# common functions
# ---------------------------------------------------------------------------
function secs_to_HMS() {
    local seconds_count="${1:-0}"
    if [ "${seconds_count}" -ge $((60*60)) ]; then
        echo -n "$((${seconds_count}/3600))h$((${seconds_count}%3600/60))m$((${seconds_count}%60))s"
    elif [ "${seconds_count}" -ge 60 ]; then
        echo -n "$((${seconds_count}%3600/60))m$((${seconds_count}%60))s"
    else
        echo -n "$((${seconds_count}%60))s"
    fi
}

function log() {
    echo -e "[$(date +'%Y-%m-%dT%H:%M:%S')] $@ (running $(secs_to_HMS ${SECONDS}))" 1>&2
}

# ostack_cli_ro [openstack-client-args]
#   openstack client read-only wrapper
function ostack_cli_ro() {
    local object_type="$1"
    local object_action="$2"

    if [ -n "${object_type}" -a -n "${object_action}" ]; then
        if [[ -n "${object_type}" && "${object_action}" =~ ^(show|list)$ ]]; then
            openstack $* -f json
        else
            return 1
        fi
    else
        cat | awk '{if ($0 ~ /.+ (show|list)/){print $0 " -f json"}}' | \
          openstack
    fi
}

function dirs_exist() {
    local file_names="$1"
    for i_dir in $(echo "${file_names}" | xargs dirname | sort -u); do
        test -d "${i_dir}" || mkdir -p "${i_dir}"
    done
}

function file_dup_create() {
  local file="$1"
  cp -f "${file}" "${file}.tmp"
}

function file_dup_remove() {
  local file="$1"
  rm -f "${file}.tmp"
}

function get_version() {
  local changelog_file="$1"

  grep -E '^##[ \t]+\[.+\]' "${changelog_file}" | \
    awk '{print substr($2,2,length($2)-2)}' | \
    grep -v '[Uu]nreleased' | head -1
}


# ostack-entity-dump.sh functions
# ---------------------------------------------------------------------------
function concat_json_split() {
    local all_file="$1"
    local output_file_fmt="$2"
    local fields="$3"
    local all_items="$(cat "${all_file}" | jq .)"
    local file_names="$(echo "${all_items}" | jq -r "${fields}" | gawk -v "fmt=${output_file_fmt}\n" -f "${STREAM_PRINTF_TOOL}")"

    dirs_exist "${file_names}"

    echo "${all_items}" | gawk -v file_names_file=<(echo "${file_names}") -f "${JSON_SPLIT_TOOL}"
}

function concat_json_split_host() {
    local all_file="$1"
    local output_file_fmt="$2"
    local fields="$3"
    local all_items="$(cat "${all_file}" | jq '{"id": .[0].Host, "State": .}')"
    local file_names="$(echo "${all_items}" | jq -r "${fields}" | gawk -v "fmt=${output_file_fmt}\n" -f "${STREAM_PRINTF_TOOL}")"

    dirs_exist "${file_names}"

    echo "${all_items}" | gawk -v file_names_file=<(echo "${file_names}") -f "${JSON_SPLIT_TOOL}"
}

function concat_json_split_on_file_names() {
    local all_file="$1"
    local file_names="$2"

    dirs_exist "${file_names}"

    cat "${all_file}" | jq . | gawk -v file_names_file=<(echo "${file_names}") -f "${JSON_SPLIT_TOOL}"
}


function concat_json_split_detect_domain() {
    local all_file="$1"
    local output_file_fmt="$2"
    local fields="$3"
    local all_items="$(cat "${all_file}" | jq .)"

    local file_fmt_fields="$(echo "${all_items}" | jq -r "${fields}" | \
        while read i_field_line; do
            i_prj_id=$(echo "${i_field_line}" | awk '{printf $1}')
            i_object_id=$(echo "${i_field_line}" | awk '{printf $2}')
            i_domain_id="${OSTACK_PRJ_TO_DOMAIN[${i_prj_id,,}]}"
            if [[ "$i_object_id" ]]; then
                echo "${i_domain_id:-"unknown-orphaned"} ${i_prj_id} ${i_object_id}";
            else
                echo "${i_domain_id:-"unknown-orphaned"} unknown-orphaned ${i_prj_id}"; # i_object_id always exists, it was uncorrectly assigned to i_prj_id
            fi
        done)"
    local file_names="$(echo "${file_fmt_fields}" | gawk -v "fmt=${output_file_fmt}\n" -f "${STREAM_PRINTF_TOOL}")"

    dirs_exist "${file_names}"

    echo "${all_items}" | gawk -v file_names_file=<(echo "${file_names}") -f "${JSON_SPLIT_TOOL}"
}


function ostack_dump_xyz() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local object_type="${3//-/ }"
    local object_type_single_word="$3"
    local jq_query_ids="$4"
    local ostack_brief_query_args="$5"
    local exit_callback_function="$6"
    local split_function="${7:-"concat_json_split"}"
    local brief_list_jq_id_query="${8:-".[].ID"}"

    ostack_cli_ro "${object_type}" list ${ostack_brief_query_args} > "${raw_entities_dir}/${object_type_single_word}s.brief.json"

    local entity_count="$(cat "${raw_entities_dir}/${object_type_single_word}s.brief.json" | jq '. | length')"
    log "INFO: ${entity_count} ${object_type} entities found (brief list)"
    [ "${entity_count}" == "0" ] && \
      return 0

    cat "${raw_entities_dir}/${object_type_single_word}s.brief.json" | jq -r "${brief_list_jq_id_query}" | \
      awk -v "object_type=${object_type}" '{print object_type " show " $1}' | \
      ostack_cli_ro > "${raw_entities_dir}/${object_type_single_word}s.detailed.json"
    local entity_count_detailed="$(cat "${raw_entities_dir}/${object_type_single_word}s.detailed.json" | jq -c '.' | wc -l)"
    log "INFO: ${entity_count_detailed} ${object_type} entities found (detailed list)"

    "${split_function}" "${raw_entities_dir}/${object_type_single_word}s.detailed.json" "${file_fmt}" "${jq_query_ids}"
    log "INFO: detailed list of ${object_type} entities split by ${split_function}()"

    if [ -n "${exit_callback_function}" ]; then
        ${exit_callback_function} "$@"
        log "INFO: ${exit_callback_function}() callback succeeded"
    fi
    log "INFO: ${object_type} entities dumped"
}

function ostack_dump_domain() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "domain" ".id"
}

function ostack_dump_flavor() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "flavor" ".id" "--long"
}

function ostack_dump_image() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "image" ".id" "--long"
}


function ostack_dump_user() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "user" '.domain_id + " " + .id' "--long"
}

function get_ostack_project_domain_association() {
    local raw_entities_dir="$1"

    source <(cat "${raw_entities_dir}/projects.brief.json" | \
             jq -r '.[] |.ID  + " " +."Domain ID"' | \
             tr '[:upper:]' '[:lower:]' | \
             awk '{printf("OSTACK_PRJ_TO_DOMAIN[%s]=\"%s\"\n",$1, $2)}')
}

function ostack_dump_project() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "project" '.domain_id + " " + .id' "--long" "get_ostack_project_domain_association"
}


function ostack_dump_quota() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    if [ ! -s "${raw_entities_dir}/projects.brief.json" ]; then
        ostack_cli_ro project list --long > "${raw_entities_dir}/projects.brief.json"
    fi

    local entity_count="$(cat "${raw_entities_dir}/projects.brief.json" | jq '. | length')"
    log "INFO: ${entity_count} quota entities found"
    [ "${entity_count}" == "0" ] && \
      return 0

    # detect project quota schema and dump detailed quotas
    local test_project_id="$(cat "${raw_entities_dir}/projects.brief.json" | jq -r '.[0].ID')"
    if ostack_cli_ro quota show ${test_project_id} | jq -c . | grep -Eq '^\{'; then
        cat "${raw_entities_dir}/projects.brief.json" | jq -r '.[].ID' | \
          awk '{print "quota show " $1}' | ostack_cli_ro > "${raw_entities_dir}/quotas.detailed.json"
    else
        cat "${raw_entities_dir}/projects.brief.json" | jq -r '.[].ID' | \
          awk '{print "quota show " $1}' | ostack_cli_ro | \
          jq --sort-keys 'map({key: .Resource, value: .Limit}) | from_entries' > "${raw_entities_dir}/quotas.detailed.json"
    fi

    # TODO: race-condition exists here as quota details does not come with project_id
    #   and ostack may have changed since projects were dumped (<dir>/projects.brief.json)
    local file_names="$(cat "${raw_entities_dir}/projects.brief.json" | jq -r '.[] | ."Domain ID" + " " + .ID' | \
                        gawk -v "fmt=${file_fmt}\n" -f "${STREAM_PRINTF_TOOL}")"

    concat_json_split_on_file_names "${raw_entities_dir}/quotas.detailed.json" "${file_names}"
}

function ostack_dump_server() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    # workarounding 1k server list limit: there's no '--long --limit 100k'
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "server" '.project_id + " " + .id' \
      "--all-projects" "" "concat_json_split_detect_domain"
}

function ostack_dump_mapping() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "mapping" ".id"
}

function ostack_dump_volume() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "volume" ".id" "--all-projects"
}

function ostack_dump_service() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "service" ".id" "--long"
}

function ostack_dump_region() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "region" ".region"
}

function ostack_dump_hypervisor() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "hypervisor" ".id" "--long"
}

function ostack_dump_loadbalancer() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "loadbalancer" '.project_id + " " + .id' \
      "" "" "concat_json_split_detect_domain" ".[].id"
}

function ostack_dump_floating_ip() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "floating-ip" '.project_id + " " + .id' \
      "--long" "" "concat_json_split_detect_domain"
}

function ostack_dump_subnet() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "subnet" '.project_id + " " + .id' \
      "--long" "" "concat_json_split_detect_domain"
}

function ostack_dump_network() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "network" '.project_id + " " + .id' \
      "--long" "" "concat_json_split_detect_domain"
}

function ostack_dump_router() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "router" '.project_id + " " + .id' \
      "--long" "" "concat_json_split_detect_domain"
}

function ostack_dump_volume_type() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "volume-type" ".id"
}

function ostack_dump_ip_availability() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "ip-availability" ".network_id" \
      "" "" "" '.[]."Network ID"'
}

function ostack_dump_role() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "role" ".id"
}

function ostack_dump_endpoint() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "endpoint" ".id"
}

function ostack_dump_aggregate() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "aggregate" ".id"
}

function ostack_dump_catalog() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "catalog" ".id" \
      "" "" "" ".[].Name"
}

function ostack_dump_host() {
    local raw_entities_dir="$1"
    local file_fmt="$2"

    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "host" ".id" \
      "" "" "concat_json_split_host" '.[]."Host Name"'
}

function ostack_dump_identity_provider() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "identity-provider" ".id"
}

function ostack_dump_usages() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local usage_type="$3"
    local usage_from_date="$4"
    local now="$(date +%Y-%m-%d)"

    log "INFO: compute usage ${usage_type} stats (${usage_from_date}...${now}) dump started"
    ostack_cli_ro usage list --start=${usage_from_date} > "${raw_entities_dir}/${usage_type}.brief.json"
    dirs_exist "${file_fmt}"
    cp -f "${raw_entities_dir}/${usage_type}.brief.json" "${file_fmt}"
    log "INFO: compute usage ${usage_type} stats (${usage_from_date}...${now}) dumped"
}

function ostack_dump_usage_last_1mo() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local start_date="$(date +%Y-%m-%d -d '1 month ago')"

    ostack_dump_usages "${raw_entities_dir}" "${file_fmt}" "usage-last-1mo" "${start_date}"
}

function ostack_dump_usage_last_3mo() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local start_date="$(date +%Y-%m-%d -d '3 month ago')"

    ostack_dump_usages "${raw_entities_dir}" "${file_fmt}" "usage-last-3mo" "${start_date}"
}

function ostack_dump_usage_last_6mo() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local start_date="$(date +%Y-%m-%d -d '6 month ago')"

    ostack_dump_usages "${raw_entities_dir}" "${file_fmt}" "usage-last-6mo" "${start_date}"
}

function ostack_dump_usage_last_12mo() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local start_date="$(date +%Y-%m-%d -d '12 month ago')"

    ostack_dump_usages "${raw_entities_dir}" "${file_fmt}" "usage-last-12mo" "${start_date}"
}

function ostack_dump_usage_ytd() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local start_date="$(date +%Y-01-01)"

    ostack_dump_usages "${raw_entities_dir}" "${file_fmt}" "usage-ytd" "${start_date}"
}

function ostack_dump_availability_zone() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    local usage_type=""
    local object_type_single_word="availability-zone"
    local object_type="${object_type_single_word//-/ }"
    local ostack_brief_query_args="--long"
    local split_function="concat_json_split"
    local jq_query_ids='."Zone Name" + "-" + ."Zone Resource" + "-" + ."Host Name"'

    ostack_cli_ro "${object_type}" list ${ostack_brief_query_args} > "${raw_entities_dir}/${object_type_single_word}s.brief.json"

    local entity_count="$(cat "${raw_entities_dir}/${object_type_single_word}s.brief.json" | jq '. | length')"
    log "INFO: ${entity_count} ${object_type} entities found (brief list)"
    [ "${entity_count}" == "0" ] && \
      return 0
    cat "${raw_entities_dir}/${object_type_single_word}s.brief.json" | jq '.[]' > "${raw_entities_dir}/${object_type_single_word}s.detailed.json"

    "${split_function}" "${raw_entities_dir}/${object_type_single_word}s.detailed.json" "${file_fmt}" "${jq_query_ids}"
    log "INFO: detailed list of ${object_type} entities split by ${split_function}()"
    log "INFO: ${object_type} entities dumped"
}

function ostack_dump_network_agent() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "network-agent" ".id"
}

function ostack_dump_port() {
    local raw_entities_dir="$1"
    local file_fmt="$2"
    ostack_dump_xyz "${raw_entities_dir}" "${file_fmt}" "port" ".id"
}
