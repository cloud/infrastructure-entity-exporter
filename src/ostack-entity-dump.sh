#!/bin/bash
# ostack-entity-dump.sh [--help|-h]
# ostack-entity-dump.sh <dump-dir> [entity] ...
#   dumps openstack entities (possibly specified as [entity] arguments to directory <dump-dir>
#
# Directory structure:
# <dump-dir>/domains
# <dump-dir>/domains/<domain-id>/users
# <dump-dir>/domains/<domain-id>/projects
# <dump-dir>/domains/<domain-id>/quotas
# <dump-dir>/domains/<domain-id>/projects/<project-id>/servers/
# <dump-dir>/domains/<domain-id>/projects/<project-id>/loadbalancers/
# <dump-dir>/domains/<domain-id>/projects/<project-id>/floating-ips
# <dump-dir>/domains/<domain-id>/projects/<project-id>/networks
# <dump-dir>/domains/<domain-id>/projects/<project-id>/subnets
# <dump-dir>/domains/<domain-id>/projects/<project-id>/routers
# <dump-dir>/...
#
# Notes:
# * if you want to export servers/loadbalancers/floating ips/networks/subnets/routers, you also need projects and domains, see the directory structure above
#
# Usage:
#   * dump (all exportable) ostack entities into directory ./openstack-dump-dir
#     source ./ostack-admin-rc.sh.inc
#     ./ostack-entity-dump.sh ./openstack-dump-dir
#   * dump ostack volume entities into directory ./openstack-volumes-dump-dir
#     source ./ostack-admin-rc.sh.inc
#     ./ostack-entity-dump.sh ./openstack-volumes-dump-dir volume

SCRIPT_DIR="$(dirname $(readlink -f $0))"
DUMP_ROOTDIR="$1"
shift
OBJECTS="$@"
[ -z "${OBJECTS}" ] && OBJECTS="domain user project image flavor server mapping quota volume service region hypervisor loadbalancer floating-ip subnet network router volume-type ip-availability role endpoint aggregate catalog host identity-provider usage-last-1mo usage-last-3mo usage-last-6mo usage-last-12mo usage-ytd availability-zone network-agent port"

set -eo pipefail

source ${SCRIPT_DIR}/lib.sh


if [ -z "${DUMP_ROOTDIR}" -o "${DUMP_ROOTDIR:0:1}" == "-" ]; then
  cat $0 | awk '$0~/^#[^!]/{print} NF==0{exit(0)}'
  [[ "${DUMP_ROOTDIR}" =~ ^-h|--help ]] && exit 0 || exit 1
fi

STEP_NAME="initialization (constants, functions)"

# lazy load ostack project to domain mapping
declare -A OSTACK_PRJ_TO_DOMAIN

# local functions
# ---------------------------------------------------------------------------
STEP_NAME="initialization (functions)"

function at_exit() {
  if [ -n "${DUMP_ROOTDIR}" -a -d "${DUMP_ROOTDIR}/__raw" ]; then
    set | grep "^OSTACK_PRJ_TO_DOMAIN" > "${DUMP_ROOTDIR}/__raw/OSTACK_PRJ_TO_DOMAIN.vardump"
  fi
  
  if [ "${STEP_NAME,,}" == "success" ]; then
	exit 0
  else
	echo "ERROR: $(basename $0) failed and aborted during the step \"${STEP_NAME}\"."
	exit 1
  fi
}
trap at_exit EXIT

# main steps
# ---------------------------------------------------------------------------
log "INFO: ostack-entity-dump.sh version: $(get_version ${SCRIPT_DIR}/CHANGELOG.md)"

STEP_NAME="openstack initial authentication test"
ostack_cli_ro versions show &>/dev/null

STEP_NAME="output directory ${DUMP_ROOTDIR} creation"
test -d "${DUMP_ROOTDIR}/__raw" || mkdir -p "${DUMP_ROOTDIR}/__raw"

log "INFO: Plan to dump: ${OBJECTS}"

for i_entity in ${OBJECTS}; do
    log "INFO: Dump ${i_entity} entities"
    STEP_NAME="openstack ${i_entity} export"
    "ostack_dump_${i_entity//-/_}" "${DUMP_ROOTDIR}/__raw" "${DUMP_ROOTDIR}/${OBJECT_FILE_FMT[${i_entity}]}"
done

STEP_NAME="Success"
